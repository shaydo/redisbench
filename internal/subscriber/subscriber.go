package subscriber

import (
	"context"
	"fmt"
	"log/slog"
	"math/rand"

	"github.com/gomodule/redigo/redis"
)

type Subscriber struct {
	c   redis.Conn
	psc redis.PubSubConn
}

func New(ctx context.Context, subs, symbols int) (*Subscriber, error) {
	c, err := redis.Dial("tcp", ":6379")
	if err != nil {
		return nil, fmt.Errorf("couldn't create redis connection: %v", err)
	}
	s := &Subscriber{
		c: c,
	}
	if err := s.subscribe(ctx, subs, symbols); err != nil {
		s.c.Close()
		return nil, fmt.Errorf("couldn't subscribe: %v", err)
	}
	go s.run(ctx)
	return s, nil
}

func (s *Subscriber) subscribe(ctx context.Context, subs, symbols int) error {
	chosen := make([]int, 0, subs)
	for i := 0; i < subs; i++ {
	loop:
		for {
			n := rand.Intn(symbols)
			for _, m := range chosen {
				if n == m {
					continue loop
				}
			}
			chosen = append(chosen, n)
			break
		}
	}

	channels := make([]string, 0, subs)
	for _, n := range chosen {
		channels = append(channels, fmt.Sprintf("SYMBOL::%d", n))
	}
	s.psc = redis.PubSubConn{Conn: s.c}
	if err := s.psc.Subscribe(redis.Args{}.AddFlat(channels)...); err != nil {
		return fmt.Errorf("subscribe request has failed: %v", err)
	}

	return nil
}

func (s *Subscriber) run(ctx context.Context) {
	for {
		switch err := s.psc.ReceiveContext(ctx).(type) {
		case error:
			slog.Error("receive returned an error", slog.Any("error", err))
			return
		}
	}
}
