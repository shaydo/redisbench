package publisher

import (
	"context"
	"fmt"
	"log/slog"
	"time"

	"github.com/gomodule/redigo/redis"
)

type Publisher struct {
	c       redis.Conn
	symbols int
}

func New(ctx context.Context, symbols int) (*Publisher, error) {
	c, err := redis.Dial("tcp", ":6379")
	if err != nil {
		return nil, fmt.Errorf("couldn't create redis connection: %v", err)
	}
	p := &Publisher{
		c:       c,
		symbols: symbols,
	}
	go p.run(ctx)
	return p, nil
}

func (p *Publisher) run(ctx context.Context) {
	ticker := time.NewTicker(time.Second)
	for {
		select {
		case <-ticker.C:
			if err := p.publishTicks(ctx); err != nil {
				slog.Error("couldn't publish ticks", slog.Any("error", err))
			}
		case <-ctx.Done():
			return
		}
	}
}

func (p *Publisher) publishTicks(ctx context.Context) error {
	if err := p.c.Send("MULTI"); err != nil {
		return fmt.Errorf("send MULTI has failed: %v", err)
	}
	updateMsg := "this is a string value that is not supposed to mean anything"
	for i := 0; i < p.symbols; i++ {
		if err := p.c.Send("SET", fmt.Sprintf("QUOTE::%d", i), updateMsg); err != nil {
			return fmt.Errorf("send SET has failed: %v", err)
		}
		if err := p.c.Send("PUBLISH", fmt.Sprintf("SYMBOL::%d", i), updateMsg); err != nil {
			return fmt.Errorf("send PUBLISH has failed: %v", err)
		}
	}
	if err := p.c.Flush(); err != nil {
		return fmt.Errorf("flush has failed: %v", err)
	}
	for i := 0; i < 2*p.symbols+1; i++ {
		reply, err := p.c.Receive()
		if err != nil {
			return fmt.Errorf("receive failed: %v", err)
		}
		switch reply.(type) {
		case error:
			return fmt.Errorf("reply %d contained an error: %v", i, reply)
		}
	}
	start := time.Now()
	_, err := p.c.Do("EXEC")
	dur := time.Since(start)
	slog.Info("EXEC returned", slog.Duration("duration", dur))
	if err != nil {
		return fmt.Errorf("EXEC has failed: %v", err)
	}
	return nil
}
