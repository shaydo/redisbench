package main

import (
	"context"
	"flag"
	"log/slog"
	"redisbench/internal/publisher"
	"redisbench/internal/subscriber"
	"time"
)

func main() {
	subs := flag.Int("subs", 10, "number of symbols to subscribe to on every connection")
	symbols := flag.Int("symbols", 300, "total number of symbols to publish")
	connections := flag.Int("connections", 200, "the number of client connections")
	flag.Parse()
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	subscribers := make([]*subscriber.Subscriber, 0, *connections)
	for i := 0; i < *connections; i++ {
		s, err := subscriber.New(ctx, *subs, *symbols)
		if err != nil {
			slog.Error("couldn't create subscriber: %v", err)
			return
		}
		subscribers = append(subscribers, s)
	}
	slog.Info("created all subscribers")

	_, err := publisher.New(ctx, *symbols)
	if err != nil {
		slog.Error("couldn't create publisher: %v", err)
		return
	}
	<-time.After(10 * time.Second)
	cancel()
}
